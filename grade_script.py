#!/usr/bin/env python3

# Made by Xavier Delattre. If you have any questions or comment, email me at xavier1.delattre@epitech.eu

import sys
import os
import csv

# usage: python3 grade_script.py import.csv nb_max_credits_module
# output: import-grades.csv


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def setGrade():
    if (len(sys.argv) != 3):
        if (len(sys.argv) == 2 and sys.argv[1] == "--help"):
            print(bcolors.WARNING + "Usage: " + bcolors.ENDC + "./grade_script.py import_file.csv nv_credit_max_module\nThis script grade " + bcolors.UNDERLINE + "automatically" + bcolors.ENDC + " a module.\nIn order to do that, you need to put the exported marks in " + bcolors.BOLD + "csv file" + bcolors.ENDC + "from the intranet with a " + bcolors.BOLD + "COMMA as delimiter AND NOTHING ELSE (like a semi-colon). " + bcolors.ENDC + "Also, when you export the marks, you need to " + bcolors.BOLD + "offuscate the headers. " + bcolors.ENDC +
                  "Which means, in the csv, you only have the students and their marks and not the descriptions of the columns on the first line.\nThe other parameter is the total credit for the module (it is used for the calculation).\nThe script will create an " + bcolors.BOLD + "output csv file " + bcolors.ENDC + "with the name as the input file except that the string \'_notes\' is replace by the string \'_grades\'. The output file is made to be exported it to the intranet.\nWhen importing the file in the intranet, you have to " + bcolors.BOLD + "specify the delemiter, " + bcolors.ENDC + "which is a " + bcolors.BOLD + "comma " + bcolors.ENDC + "and not the default semi-colon.\n")
        else:
            print(bcolors.WARNING + "Usage: " + bcolors.ENDC + "./grade_script.py import_file.csv nb_credit_max_module\n" +
                  bcolors.BOLD + "For more information, type: ./grade_script.py --help\n" + bcolors.ENDC)
        return -1
    else:
        if (sys.argv[1].find("_notes.csv") == -1):
            print(bcolors.WARNING + "Error: " + bcolors.ENDC +
                  "The file you want to use is not a csv file exported from the intranet. Please verify that is the right file or you did not change the name of the file.\n")
            return -1
        nonAlphaModule = ""
        while True:
            nonAlphaModule = input(
                'Is the module you want to grade a type \"Acquis/Echec\" module? (Yes/No):\n').lower()
            if (nonAlphaModule == 'yes' or nonAlphaModule == 'no'):
                break
            else:
                print('Please enter \"Yes\" or \"No\"')
        with open(sys.argv[1], mode='r', encoding='utf_8_sig') as csvImport:
            students = csv.reader(csvImport)
            with open(sys.argv[1].replace("_notes", "_grades"), mode='w') as outputFile:
                outputWriter = csv.writer(
                    outputFile, quotechar='"', quoting=csv.QUOTE_MINIMAL)
                outputWriter.writerow(
                    ['login', 'grade', 'credits', 'difficulty', 'medal', 'ghost'])
                for row in students:
                    lenght = len(row)
                    nb = 0
                    avg = 0.0
                    maxMark = 0.0
                    credit = int(sys.argv[2])
                    half = credit / 2
                    unit = half / 4
                    if (row[0]):
                        i = 0
                        while (i != len(row[0])):
                            if (row[0][i] == ";"):
                                print(bcolors.WARNING + "Error: " + bcolors.ENDC +
                                      "The delimiter of the input csv file is a semicolon and not a comma. To make the script works, please use the good delimiter.\n")
                                if (os.path.exists(sys.argv[1].replace("_notes", "_grades"))):
                                    os.remove(sys.argv[1].replace(
                                        "_notes", "_grades"))
                                return -1
                            i += 1
                        i = 1
                    if (row[0].lower() == "login"):
                        continue
                    while (i != lenght):
                        if (row[i] == ''):
                            nb += 1
                            i += 1
                        elif(row[i].lower() == "absent" or row[i].lower() == "n/a"):
                            i += 1
                        else:
                            if (float(row[i]) > maxMark):
                                maxMark = float(row[i])
                            avg = avg + float(row[i])
                            nb += 1
                            i += 1
                    if (avg > 0):
                        avg = avg / nb
                    else:
                        avg = 0
                    if (sys.argv[1].find("W-COL-501") != -1 or sys.argv[1].find("W-COL-502") != -1):
                        avg = maxMark
                    if (nb == 0):
                        print(bcolors.WARNING + "[Warning] " + bcolors.ENDC + "The login " + bcolors.BOLD + row[0] + bcolors.ENDC +
                              " is marked as absent on all the marks for the module. Therefore, this login will not be graded.")
                    if (nonAlphaModule == "yes"):
                        if (avg >= 10):
                            outputWriter.writerow(
                                [row[0], 'Acquis', int(credit), '0', '0', '0'])
                        else:
                            outputWriter.writerow(
                                [row[0], 'Echec', '0', '0', '0', '0'])
                    else:
                        if (avg >= 15):
                            if (avg >= 16):
                                outputWriter.writerow(
                                    [row[0], 'A', int(credit), '0', '1', '0'])
                            else:
                                outputWriter.writerow(
                                    [row[0], 'A', int(credit), '0', '0', '0'])
                        elif (avg >= 13 and avg < 15):
                            credit = half + unit * 3
                            outputWriter.writerow(
                                [row[0], 'B', int(credit), '0', '0', '0'])
                        elif (avg >= 10 and avg < 13):
                            credit = half + unit * 3
                            outputWriter.writerow(
                                [row[0], 'C', int(credit), '0', '0', '0'])
                        elif (avg >= 8 and avg < 10):
                            credit = half + unit
                            outputWriter.writerow(
                                [row[0], 'D', int(credit), '0', '0', '0'])
                        else:
                            credit = 0
                            if (avg == 0):
                                outputWriter.writerow(
                                    [row[0], 'Echec', credit, '0', '0', '1'])
                            elif (avg <= 5 and avg != 0):
                                outputWriter.writerow(
                                    [row[0], 'Echec', credit, '1', '0', '0'])
                            else:
                                outputWriter.writerow(
                                    [row[0], 'Echec', credit, '0', '0', '0'])
    print(bcolors.OKGREEN + "All the students have been graded. You can find the output to import to the intranet in the file: " +
          bcolors.ENDC + bcolors.BOLD + sys.argv[1].replace("_notes", "_grades") + bcolors.ENDC)
    return 0


setGrade()

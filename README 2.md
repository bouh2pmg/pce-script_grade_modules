# Grade Script - Web@cadémie

## Introduction

The script in this repository give the possibility to grade automatically modules for the intranet. It should works for any formation but keep in mind that it was made specifically for the Web@cadémie. Which means that the only special modules managed are `W-COL-501` and `W-COL-502`.  
The script is written in Python3 and don't use any other dependency.

## How to use

_Note: in case you forget how to use it, you can simply run it without any argument (`./grade_script.py`) and it will print you a usage. Or to have more information, you can use the help option (`./grade_script.py --help`)._

### Preparation

First step you need to have a CSV file from the intranet with all the marks of the students. You can choose to have headers or not **BUT you need to absolutely have a COMMA as delimiter and nothing else** (the script will tell you if you choosed the wrong one).  
Please be careful to not change the default name given by the intranet or it could end up in error.

### Launch the script

You have to run the script as following:

```bash
./grade_script.py import_file.csv nb_max_credits_module
```

Which means that for example if you want to grade the module `W-MUL-130` (Photoshop) you will use the script as following:

```bash
./grade_script.py \[W-MUL-130\]\[LYN-2-1\]_notes.csv 2
```

_Note that you need to escape the character `[` and the character `]` whith a backslash._

### Running phase

While running, the script will ask you if the module you want to grade is a module where the grades are `Acquis` or `Echec`, even if it is special modules like `W-COL-501` and you will have to answer directly on the command line.

### Absence

The script considered that if there is no mark for a student on one column, the absence is not justified therefore, it will be considered as a 0.  
A student will be considered as absent if in the column it's written `absent` the script will not count this one and pass to the next one.  
\_Note: if a student is `absent` on all the columns, he will not be graded and the script will send you a message to inform you.

### Output

When the script finish to run, it will print you an output saying that it graded the module with the name of the output file.  
For example, for the module `W-MUL-130` (with the default name of the input file), the output file will have the name : `[W-MUL-130][LYN-2-1]_grades.csv`.  
As the input file, the delimiter of the output file will be a comma.  
You can directly import the output file in the intranet without more modifications.  
_Note: the medal, ghost etc are generated in the output file as well._

### Error management

A good coverage of error cases are managed.
You have output colored messages if an error occured or not and the following are managed:

- The input file is not a csv
- The input file is not a csv from the intranet (file name verification)
- The delimiter in the input file. If it is not a comma or a mix between different delimiters
- The script can manage headers or not
- The output file has the same name of the input file with `_grades.csv` at the end compare to `_notes.csv`of the input file
- What you can enter in the command line when the script asks you if the module is a `Acquis/Echec` one

## Final note

Thank you for using my script. You can use it or modify it without any restriction, there is no license attach to it. If you have any question or comment, you can email me at xavier1.delattre@epitech.eu
